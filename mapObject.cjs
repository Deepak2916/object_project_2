const keys=require('./keys.cjs')

const mapObject=(obj,cb)=>{
    const typeObj = typeof (obj)
    const typeCb=typeof(cb)
    if (typeObj == undefined || typeObj !== 'object' || Array.isArray(obj) || typeCb!=='function') {
        return {}
    }
    let result={}
    const objKeys=keys(obj)
    // console.log(objKeys)
    for(let index=0;index<objKeys.length;index++){
        let [key,val]=[objKeys[index],obj[objKeys[index]]]

        result[key]=cb(val,key)

        // console.log(result[key])


    }
    return result

}

module.exports=mapObject