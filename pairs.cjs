const keys=require('./keys.cjs')

const pairs=(obj)=>{
    const typeObj = typeof (obj)
    
    if (typeObj=='number' || typeObj=='function') {
        return []
    }
    let result=[]
    const objKeys=keys(obj)
    // console.log(objKeys)
    for(let index=0;index<objKeys.length;index++){
        let [key,val]=[objKeys[index],obj[objKeys[index]]]

        result[index]=[key,val]

        // console.log(result[key])


    }
    return result

}

module.exports=pairs