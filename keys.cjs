const keys = (obj) => {
    let keysArray = []
    const typeObj = typeof (obj)
    if (typeObj=='number' || typeObj=='function') {

        return []
    }

    // let keysArray = []
    for (let key in obj) {
        keysArray.push(key)
    }
    return keysArray
}

module.exports = keys