const keys = require('./keys.cjs')

const invert = (obj) => {
    const typeObj = typeof (obj)

    if (typeObj == undefined || typeObj !== 'object' || Array.isArray(obj)) {
        return {}
    }
    let result = {}
    const objKeys = keys(obj)
    // console.log(objKeys)
    for (let index = 0; index < objKeys.length; index++) {
        let [key, val] = [objKeys[index], obj[objKeys[index]]]

        result[val] = key

        // console.log(result[key])


    }
    return result

}

module.exports = invert