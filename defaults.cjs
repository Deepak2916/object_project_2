const keys = require('./keys.cjs')

const DefaultObj = (obj, defaultObj) => {
    const typeObj = typeof (obj)
    const typeofDefault = typeof (defaultObj)


    if (typeObj == undefined || typeObj !== 'object' || Array.isArray(obj)) {
        return {}
    }
    else if (typeofDefault == undefined || typeofDefault !== 'object' || Array.isArray(typeofDefault)) {
        return obj
    }

    const defaultObjKeys = keys(defaultObj)
    const objKeys = keys(obj)
    // console.log(defaultObjKeys)
    for (let index = 0; index < defaultObjKeys.length; index++) {
        if (!objKeys.includes(defaultObjKeys[index])) {
            let [key, val] = [defaultObjKeys[index], defaultObj[defaultObjKeys[index]]]
            obj[key] = val
        }
        // console

        

        // console.log(result[key])


    }
    return obj

}

module.exports = DefaultObj